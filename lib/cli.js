"use strict";

var path = require('path');
var fs = require('fs');
var streamify = require('stream-array');
var yargs = require('yargs');
var File = require('vinyl');
var vinylFile = require('vinyl-file');
var grayMatter = require('gulp-gray-matter');
var addFrontMatter = require('gulp-add-front-matter');
var plugin = require('gulp-add-missing-post-images');
var dotted = require('dotted');

function makeImage(argv) {
    // When we are creating an image, we fake the data and then run it through
    // the pipe.
    var fakeFile = new File({
        title: argv.text,
        path: 'bob.html'
    });
    var files = [];

    streamify([fakeFile])
        .pipe(plugin(argv))
        .on('data', function(file) { files.push(file); })
        .on('end', function() {
            var image = files[0];
            fs.writeFileSync(argv.output, image.contents);
        });
}

function updatePost(argv) {
    // Normalize the path.
    argv.image = "data." + argv.image;
    argv.title = "data." + argv.title;

    // Load the input post into memory.
    var postFile = vinylFile.readSync(argv.inputFile);
    var files = [];

    streamify([postFile])
        .pipe(grayMatter({ property: "data" }))
        .pipe(plugin(argv))
        .pipe(addFrontMatter({ property: "data" }))
        .on('data', function(file) {
            files.push(file);
        })
        .on('end', function() {
            // Pull out the two files from the stream regardless of order.
            var post = files.filter(f => f.path.indexOf(".png") < 0)[0];
            var image = files.filter(f => f !== post)[0];

            // If we have an image, then write out everything. Otherwise,
            // nothing changed.
            if (image) {
                // Figure the output files to use.
                var imageName = dotted.getNested(post, argv.image);
                var imagePath = path.join(argv.directory, imageName);
                var postName = path.basename(post.path);
                var postPath = path.join(argv.directory, postName);

                // Write out both of them.
                fs.writeFileSync(postPath, post.contents + "\n");
                fs.writeFileSync(imagePath, image.contents);
            }
        });
}

function imageOptions(args) {
    return args
        .option('background', { alias: 'b', default: 0x00000000 })
        .option('size', { alias: 's', default: 256 })
        .option('rows', { alias: 'r', default: 1 })
        .option('columns', { alias: 'c', default: 1 })
        .option('padding', { default: 0 })
	    .option('prefix', { default: '' })
        .option('spacing', { default: 0 })
        .option('output', { alias: 'o', default: 'image.png' })
        .option('image', { default: 'image' })
        .option('title', { default: 'title' });
}

function fileOptions(args) {
    return imageOptions(args)
        .option('directory', { alias: 'd', default: '.' });
}

var args = yargs
    .usage('$0 <cmd> [args]')
    .command(
        'image <text>',
        'Create an image for a given text.',
        imageOptions,
        makeImage)
    .command(
        'file <inputFile>',
        'Create an image for a given text.',
        fileOptions,
        updatePost)
    .demandCommand(2)
    .help()
    .argv;
