gulp-add-missing-post-images-cli
================================

> A command-line tool for creating unique images based on contents in a Markdown with YAML file or updating them.

## Setup and usage

Install `gulp-add-missing-post-images-cli` using `npm`:

```sh
npm i gulp-add-missing-post-images-cli
```

## Generating images

```sh
add-missing-post-images image "Title of Image"
```

## Updating files

```sh
add-missing-post-images file input.md
```
